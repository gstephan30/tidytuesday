library(tidyverse)
df <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2018-12-04/medium_datasci.csv")


df %>% 
  count(publication, sort = TRUE)

df %>% 
  summarise_at(vars(starts_with("tag_")), sum)


# TAGS
df %>% 
  gather(tag, value, starts_with("tag_")) %>% 
  mutate(tag = str_remove(tag, "tag_")) %>% 
  filter(value == 1) -> df.gathered

df.gathered %>% 
  count(tag, sort = TRUE)

df.gathered %>% 
  group_by(tag) %>% 
  summarise(median_claps = median(claps),
            total_claps  = sum(claps)) %>% 
  arrange(desc(median_claps))

df %>%
  ggplot(aes(claps)) +
  geom_histogram() +
  scale_x_log10(labels = scales::comma_format())

# Reading Time
df %>% 
  # pmin gatheres all observations together
  mutate(reading_time = pmin(10, reading_time)) %>% 
  ggplot(aes(reading_time)) +
  geom_histogram(binwidth = 0.5) +
  scale_x_continuous(breaks = seq(2, 10, 2),
                     labels = c(seq(2, 8, 2), "10+")) + 
  cowplot::theme_cowplot() +
  labs(x = "Medium reading time")


### Textmining
library(tidytext)

df %>% 
  select(title, subtitle, year, reading_time, claps) %>% 
  filter(!is.na(title)) %>% 
  unnest_tokens(word, title) %>% 
  anti_join(stop_words, by = "word") %>% 
  filter(word != "de",
         str_detect(word, "[a-z]")) %>% 
  mutate(word = str_replace(word, "[[:punct:]]", "")) %>% 
  count(word, sort = TRUE) %>% 
  head(20) %>% 
  ggplot(aes(reorder(word, n), n)) +
  geom_col() +
  coord_flip() +
  labs(title = "Common words in Medium post titles")


df %>% 
  select(title, subtitle, year, reading_time, claps) %>% 
  filter(!is.na(title)) %>% 
  unnest_tokens(word, title) %>% 
  anti_join(stop_words, by = "word") %>% 
  filter(word != "de",
         str_detect(word, "[a-z]")) %>% 
  mutate(word = str_replace(word, "[[:punct:]]", "")) %>% 
  group_by(word) %>% 
  summarise(median_claps = median(claps),
            geommetric_mean = exp(mean(log(claps + 1))) - 1,
            occurence = n()) %>% 
  filter(occurence >= 500)


library(widyr)
library(stopwords)
top_words_cors <- df %>% 
  transmute(post_id = row_number(), title, subtitle, year, reading_time, claps) %>% 
  filter(!is.na(title)) %>% 
  unnest_tokens(word, title) %>% 
  anti_join(stop_words, by = "word") %>% 
  anti_join(as_tibble(stopwords("es")), by = c("word" = "value")) %>% 
  anti_join(as_tibble(stopwords("de")), by = c("word" = "value")) %>% 
  filter(str_detect(word, "[a-z]")) %>% 
  mutate(word = str_replace(word, "[[:punct:]]", "")) %>% 
  select(post_id, word) %>% 
  add_count(word) %>% 
  filter(n >= 500) %>% 
  pairwise_cor(word, post_id, sort = TRUE) %>% 
  head(50)

library(ggraph)
library(igraph)

set.seed(2018)
top_words_cors %>% 
  graph_from_data_frame() %>% 
  ggraph(layout = "fr") +
  geom_edge_link() +
  geom_node_point(color = "lightblue", size = 5) +
  geom_node_text(aes(label = name), repel = TRUE) + # repel keeps the words from overlapping
  theme_void()

### lasso
### https://youtu.be/C69QyycHsgE
